import React, { FC } from 'react'
import { user } from './types/user'

interface UserProps {
  user:user;
}
const User:FC<UserProps> = (props) => {
  const {user}=props;
  return (
    <dl>
        <dt>name</dt>
        <dd>{user.name}</dd>
        <dt>hobbies</dt>
        <dd>{user.hobbies?.join("/")}</dd>
    </dl>
  )
}

export default User