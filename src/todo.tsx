import React from 'react'
import { TodoType } from './types/todo';

//utilise Omit
const Todo = (props:Omit<TodoType,"id">) => {
    const {title, userId, completed=false} = props;
    const completedMark = completed?"(finished)":"(unfinished)";

    return (
        <p>{`${completedMark}:title:${title}By${userId}`}</p>
    )
}

//exemple of Pick
/* const Todo = (props:Pick<TodoType,"title"|"userId"|"completed">) => {
    const {title, userId, completed=false} = props;
    const completedMark = completed?"(finished)":"(unfinished)";

    return (
        <p>{`${completedMark}:title:${title}By${userId}`}</p>
    )
} */

export default Todo