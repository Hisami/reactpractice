import axios from 'axios';
import { userInfo } from 'os';
import { title } from 'process';
import React, { useState } from 'react';

import './App.css';
import Text from './Text';
import Todo from './todo';
import { TodoType } from './types/todo';
import User from './User';
const user = {
  name:"hana",
  hobbies:["sports","drawing"]
}
function App() {
  const [todos, setTodos] = useState<TodoType[]>([]);
  const fetchAPI = ()=>{
  console.log("hello")
  axios.get<TodoType[]>("https://jsonplaceholder.typicode.com/todos").then((res)=>{
    setTodos(res.data);
  }
  )
  }
  return (
    <div className="App">
      <User user={user}/>
      <Text color="red" fontWeight="bold"></Text>
      <button onClick={fetchAPI}>get Data</button>
      {todos.map((todo)=>(
      <Todo key={todo.id} title={todo.title} userId={todo.userId} completed={todo.completed}/>
      ))}
    </div>
  );
}

export default App;
