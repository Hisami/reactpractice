import React, { FC } from 'react'

interface TextProps {
    color: string;
    fontWeight: string
}

const Text:FC<TextProps> = (props) => {
    const {color, fontWeight} = props;
    return (
        <div style={{ color, fontWeight}}>Text</div>
    )
}

export default Text